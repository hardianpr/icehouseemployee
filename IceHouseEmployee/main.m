//
//  main.m
//  IceHouseEmployee
//
//  Created by Hardian Prakasa on 5/23/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
