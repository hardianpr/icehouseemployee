//
//  AppDelegate.h
//  IceHouseEmployee
//
//  Created by Hardian Prakasa on 5/23/17.
//  Copyright © 2017 Hardian Prakasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

